import menuburger from "../image/menu-btn.png";
import menuburgergrey from "../image/menu-btn-grey.png";
import { Link, useLocation } from "react-router-dom";

function burgerMenu(){
  console.log('coucou');
  document.querySelector(".nav-items").classList.toggle('mobile-menu')
}

export default function Header(){
    let pathname = useLocation().pathname
    return(
    
    <nav className="navbar">
        <a href="#" className="logo">
            Alcaline
        </a>
        
        <div className="nav-items">
            <ul>
                <li>
                    <Link to="/" className="home">
                        Acceuil
                    </Link>
                </li>
                <li>
                    <Link to="/Presentation">
                        Présentation
                    </Link>
                </li>
                <li>
                    <Link to="/TerreEnfumee">
                        Terre Enfumée
                    </Link>
                </li>
                <li>
                    <Link to="/TerreVernissee">
                        Terre Vernisée
                    </Link>
                </li>
                <li>
                    <Link to="/Atelier">
                        L'altelier
                    </Link>
                </li>
            </ul>
        </div>
        <img className="burger" onClick={burgerMenu} src={((pathname.includes('Vernissee'))||(pathname.includes('Atelier')))? menuburgergrey : menuburger} alt="Menu hamburger" />
    </nav>    
    )
}

import React from "react";
import data from '../data.json';

export const Presentation = () => {
  const sophie = data.presentation.sophie;
  const antoine = data.presentation.antoine;

  return (
    <>
    <style>{`body{background: black}`}</style>
      <div className="pres_container">
        <div className="paragraph1"> 
          <h3>{sophie.title}</h3>
          <p>{sophie.paragraph}</p>
        </div> 

        <div className="paragraph2">
          <h3>{antoine.title}</h3>
          <p>{antoine.paragraph}</p>
        </div>
      </div>
    </>
  )
}
import React from "react";
import homePic from '../image/profil.png';
import insta from '../image/instagram.png';
import data from "../data.json";


export default function Home(){
    const { mainTitle,subtitle,profile_image,social_media} = data.home;
    const { instagram,email,studio } = data.home.social_media;
    const {telephone,adress} = data.footer;
  
    return (
    <>
        <style>{`body{background: black}`}</style>

        <div className="home_container">
            <div className="teinte"></div>
            <div className="title">
                <h1>{mainTitle }</h1>
                <h3>{subtitle}</h3>
                <img className="Accueil_pic" src={profile_image} alt={mainTitle}/>
            </div>
  
            <div className="atTheFoot">
                <div className="right">
                    <img className="insta_Icon" src={instagram.icon} alt={mainTitle}/>
                    <p className="insta">{instagram.username}</p>
                </div>
                <div className="center">
                    <p className="studioTxt">{studio}</p>
                    <p>{mainTitle}</p>
                </div>
                <div className="left">
                    <a className="email" href={`mailto:${email}`} >{email}</a>
                </div>
            </div>
        </div>
    </>
    );
};
  
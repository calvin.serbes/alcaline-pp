import { Carousel } from 'react-carousel-minimal';

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
  return images;
}
const images = importAll(require.context('../../public/image/terre_enfumer/', false, /\.(png|jpe?g|svg)$/));

export default function TerreEnfumee() {

  let data = []
  for(let i in images){
    data.push({image: `/image/terre_enfumer/${i}`})
  }

  return (
    <div className="App">
      <div style={{ textAlign: "center" }}>
        <div style={{
          padding: "0 20px"
        }}>
          <style>{`body{background: black;}`}</style>
          <Carousel
            data={data}
            time={4000}
            width="850px"
            height="500px"
            slideNumber={true}
            automatic={true}
            dots={true}
            pauseIconColor="white"
            pauseIconSize="40px"
            slideBackgroundColor="black"
            slideImageFit="contain"
            thumbnails={true}
            thumbnailWidth="350px"
            style={{
              textAlign: "center",
              maxWidth: "850px",
              margin: "40px auto",
            }}
          />
        </div>
      </div>
    </div>
  );
}




import insta from "../image/instagram.png";

export default function Footer(){
    return(
        <footer className="footer">
            <div className="left_footer">
                <img className="insta" src={insta}></img>
                <p>@atelier.lassagne.muller</p>
            </div>
            <div className="center_footer">
                <p>Studio</p>
                <p>Sophie Lassagne</p>
                <p></p>
            </div>
            <div className="right_footer">
                <p>sophielassagne@gmail.com</p>
                <p></p>
            </div>
        </footer>
    )
}
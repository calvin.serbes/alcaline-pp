import { Carousel } from 'react-carousel-minimal';


function importAll(r) {
  let images = {};
  r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
  return images;
}

const images = importAll(require.context('../../public/image/terre_vernisser/', false, /\.(png|jpe?g|svg)$/));

export default function TerreVernissee() {

  let data = []
  for(let i in images){
    data.push({image: `/image/terre_vernisser/${i}`})
  }
   
  return (
    <div className="App_b">
      <div style={{ textAlign: "center" }}>
        <div style={{
          padding: "0 20px"
        }}>
          <style>{`body{background: white;}`}</style>
          <style>{`.navbar{background: #fbfbfb ;}`}</style>
          <style>{`a{color: #404040;}`}</style>
          <style>{`.footer{background: rgb(215, 215, 215);}`}</style>
          <style>{`.footer p{color: #404040`}</style>
          <style>{`@media screen and (max-width: 900px){
            .nav-items{
              background:rgba(255, 255, 255, 0.595)
            }
          }`}</style>
          <Carousel
            data={data}
            time={4000}
            width="850px"
            height="500px"
            slideNumber={true}
            automatic={true}
            dots={false}
            pauseIconColor="white"
            pauseIconSize="40px"
            slideBackgroundColor="white"
            slideImageFit="contain"
            thumbnails={true}
            thumbnailWidth="350px"
            style={{
              textAlign: "center",
              maxWidth: "850px",
              margin: "40px auto",
            }}
          />
        </div>
      </div>
    </div>
  );
}
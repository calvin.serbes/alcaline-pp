import './App.css';
import './composant/Header.js';
import Header from './composant/Header';
import Home from "./composant/Home";
import Footer from './composant/Footer';
import { Routes, Route } from "react-router-dom";
import { Presentation } from './composant/Presentation';
import TerreEnfumee from './composant/TerreEnfumee';
import TerreVernissee from './composant/TerreVernissee';
import Atelier from './composant/Atelier';

function App() {
  return (
    <div>
    <Header></Header>
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/Presentation" element={<Presentation/>}/>
      <Route path="/TerreEnfumee" element={<TerreEnfumee/>}/>
      <Route path="/TerreVernissee" element={<TerreVernissee/>}/>
      <Route path="/Atelier" element={<Atelier/>}/>
    </Routes>
    <Footer></Footer>
    </div>

  );
}

export default App;

# from PIL import Image

# img = Image.open('./images/peack.jpg')

# resized_img = img.resize((400, 400))

# resized_img.save('./resized_imageFiles/resized_image.jpg')

# resized_img.show()

from PIL import Image
import pathlib

size = (1000, 1000)

for input_img_path in pathlib.Path("image/atelier").iterdir():
    if input_img_path.is_file() and input_img_path.suffix.lower() in ['.jpg', '.jpeg', '.png', '.bmp','.svg',]:
        with Image.open(input_img_path) as im:
            if im.size[0] <= size[0] and im.size[1] <= size[1]:
                im.save(input_img_path)
            else:
                im.thumbnail(size)
                im.save(input_img_path)
        print(f"Processing file {input_img_path} done...")